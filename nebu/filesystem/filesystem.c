#include "filesystem/nebu_filesystem.h"

#include <sys/stat.h>

void initFilesystem(int argc, const char *argv[]) {
	dirSetup(argv[0]);
}

int fileExists(const char *path) {
  struct stat st;
  if(stat(path, &st) == 0)
    return 1;
  return 0;
}
