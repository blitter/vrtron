#version 120
  
uniform sampler2D	texture;

uniform float pixelCenter;
uniform vec2 screenSize;
uniform vec2 lensCenterOffset;
uniform vec2 screenCenterOffset;
uniform vec2 scale;
uniform vec2 scaleIn;
uniform vec4 hmdWarpParam;

// Scales input texture coordinates for distortion.
vec2 hmdWarp(vec2 in01, vec2 lensCenter)
{
	vec2 theta = (in01 - lensCenter) * scaleIn;	// Scales to [-1, 1]
	float rSq = theta.x * theta.x + theta.y * theta.y;
	vec2 rvector = theta * (hmdWarpParam.x +
							hmdWarpParam.y * rSq +
							hmdWarpParam.z * rSq * rSq +
							hmdWarpParam.w * rSq * rSq * rSq);
	return lensCenter + scale * rvector;
}

void main()
{
	vec2 lensCenter = gl_FragCoord.x < pixelCenter ? lensCenterOffset : vec2(1.0 - lensCenterOffset.x, lensCenterOffset.y);
	vec2 screenCenter = gl_FragCoord.x < pixelCenter ? screenCenterOffset : vec2(1.0 - screenCenterOffset.x, screenCenterOffset.y);
	
	vec2 oTexCoord = gl_FragCoord.xy / screenSize;

	vec2 tc = hmdWarp(oTexCoord, lensCenter);
	if (any(bvec2(clamp(tc, screenCenter - vec2(0.25, 0.5), screenCenter + vec2(0.25, 0.5)) - tc)))
	{
		gl_FragColor = vec4(0.0);
	}
	else
	{
		gl_FragColor = texture2D(texture, tc);
	}
}
