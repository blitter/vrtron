#include "game/gltron.h"
#ifdef VR
#include "vr/vr.h"
#endif

static int coffset;

char *credits[] = {
  "",
  "   GLtron is written by " "\x03" "4" "Andreas Umbach",
  "",
  " Contributors:",
  " Programming: Darrell Walisser  Nicolas Deniaud",
  "              Todd Kirby  Andy Howe  Jon Atkins",
  " Art:         Nicolas Zimmermann",
  "              Charles Babbage       Tracy Brown",
  "              Tyler Esselstrom       Allen Bond",
  " Music:       Peter Hajba",
  " Sound:       Damon Law",
  " VR support:  Keith Kaisershot",
  "",
  "Additional Thanks to:",
  "Xavier Bouchoux     Mike Field      Steve Baker",
  "Jean-Bruno Richard             Andrey Zahkhatov",
  "Bjonar Henden   Shaul Kedem    Jonas Gustavsson",
  "Mattias Engdegard     Ray Kelm     Thomas Flynn",
  "Martin Fierz    Joseph Valenzuela   Ryan Gordon",
  "Sam Lantinga                   Patrick McCarthy",
  "",
  "Thanks to my sponsors:",
  "  3dfx:              Voodoo5 5500 graphics card",
  "  Right Hemisphere:  3D exploration software",
  NULL
};

void mouseCredits (int buttons, int state, int x, int y)
{
	if ( state == SYSTEM_MOUSEPRESSED ) {
		SystemExit();
		exit(0);
	}
}

void keyCredits(int state, int k, int x, int y)
{
	if(state == SYSTEM_KEYSTATE_UP)
		return;
  SystemExit();
	exit(0);
}

void idleCredits(void) {
  scripting_RunGC();
  SystemPostRedisplay();
}

void drawCredits(void) {
  int time;
  int x, y;
  int h;
  int i;
  float colors[][3] = { { 1.0, 0.0, 0.0 }, { 1.0, 1.0, 1.0 } };
  time = SystemGetElapsedTime() - coffset;

  rasonly(gScreen);
  h = gScreen->vp_h / (24 * 3 / 2);
  for(i = 0; i < time / 250; i++) {
    glColor3fv(colors[i % 2]);
    if(credits[i] == NULL) 
      break;
    x = 10;
    if(gScreen->vp_h / gScreen->vp_w < 0.75f)
      x += (gScreen->vp_w - (gScreen->vp_h / 0.75f)) / 2.0f;
    y = gScreen->vp_h - 3 * h * (i + 1) / 2;
    drawText(gameFtx, x, y, h, credits[i]);
  }
}
void displayCredits(void) {
#ifdef VR
  if (vrEnabled) {
    setupVRGL(gScreen);
  }
#endif
  
  glClearColor(.0, .0, .0, .0);
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
#ifdef VR
  do {
#endif
    drawCredits();
    
#ifdef VR
    if (vrEnabled) {
      finishVRGL(gScreen);
    }
  } while (vrEnabled && vrEye != 0);
#endif
  
  SystemSwapBuffers();
}

void initCredits(void) {
  coffset = SystemGetElapsedTime();
}

Callbacks creditsCallbacks = { 
  displayCredits, idleCredits, keyCredits, initCredits, 
  NULL, NULL, mouseCredits, NULL, "credits"
};
