#ifndef GRAPHICS_UTILITY_H
#define GRAPHICS_UTILITY_H

#include "video.h"

extern void checkGLError(const char *where);
extern void rasonly(Visual *d);
extern void doOrtho(float l, float r, float b, float t, float n, float f);
extern void doViewport(float x, float y, float width, float height);
extern void doPerspective(float fov, float ratio, float znear, float zfar);
extern void doLookAt(float *cam, float *target, float *up);
extern void drawText(FontTex* ftx, int x, int y, int size, const char *text);
extern char* readShaderFile(const char* shaderPath);
extern unsigned int loadShaders(const char* vertexPath, const char* fragmentPath);

extern void (*orthoFunc)(float l, float r, float b, float t, float n, float f);
extern void (*viewportFunc)(float x, float y, float width, float height);
extern void (*perspectiveFunc)(float fov, float ratio, float znear, float zfar);
extern void (*lookAtFunc)(float *cam, float *target, float *up);

#endif
