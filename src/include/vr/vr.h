#ifndef VR_H
#define VR_H

#ifdef VR

#ifdef __cplusplus
extern "C" {
#endif

#include "video/video.h"

/* globals */

extern int vrEnabled;
extern int vrEye;

/* vr support -> vr.cpp */
extern void vrOrtho(float l, float r, float b, float t, float n, float f);
extern void vrViewport(float x, float y, float width, float height);
extern void vrPerspective(float fov, float ratio, float znear, float zfar);
extern void vrLookAt(float *cam, float *target, float *up);

extern void initVR(void);
extern void shutdownVR(void);

extern void setupVRGL(Visual* d);
extern void finishVRGL(Visual* d);

#ifdef __cplusplus
}
#endif

#endif

#endif
