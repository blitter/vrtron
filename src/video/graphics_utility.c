#if defined(__APPLE__)
#include <GLEW/GLEW.h>
#else
#ifdef GLEW_STATIC
#include "GL/glew.h"
#else
#include <GL/glew.h>
#endif
#endif

#include "video/video.h"
#include "game/game.h"
#include "vr/vr.h"

#include "Nebu_filesystem.h"

void (*orthoFunc)(float l, float r, float b, float t, float n, float f) = doOrtho;
void (*viewportFunc)(float x, float y, float width, float height) = doViewport;
void (*perspectiveFunc)(float fov, float ratio, float znear, float zfar) = doPerspective;
void (*lookAtFunc)(float *cam, float *target, float *up) = doLookAt;

void checkGLError(const char *where) {
  int error;
  error = glGetError();
  if(error != GL_NO_ERROR)
    printf("[glError: %s] - %d\n", where, error);
}

void rasonly(Visual *d) {
  /* do rasterising only (in local display d) */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  orthoFunc(0.0f, (GLfloat) d->vp_w, 0.0f, (GLfloat) d->vp_h, 0.0f, 1.0f);    
  checkGLError("rasonly");
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  viewportFunc(d->vp_x, d->vp_y, d->vp_w, d->vp_h);
}

void doOrtho(float l, float r, float b, float t, float n, float f) {
  glOrtho(l, r, b, t, n, f);
}

void doViewport(float x, float y, float width, float height) {
  glViewport(x, y, width, height);
}

void doPerspective(float fov, float ratio, float znear, float zfar) {
  float top;
  float left;

  top = tanf( fov * PI / 360.0 ) * znear;
  left = - top * ratio;
  glFrustum(left, -left, -top, top, znear, zfar);
}

void doLookAt(float *cam, float *target, float *up) {
  float m[16];
  float x[3], y[3], z[3];

  vsub(cam, target, z);
  normalize(z);
  crossprod(up, z, x);
  crossprod(z, x, y);
  normalize(x);
  normalize(y);

#define M(row,col)  m[col*4+row]
  M(0,0) = x[0];  M(0,1) = x[1];  M(0,2) = x[2];  M(0,3) = 0.0;
  M(1,0) = y[0];  M(1,1) = y[1];  M(1,2) = y[2];  M(1,3) = 0.0;
  M(2,0) = z[0];  M(2,1) = z[1];  M(2,2) = z[2];  M(2,3) = 0.0;
  M(3,0) = 0.0;   M(3,1) = 0.0;   M(3,2) = 0.0;   M(3,3) = 1.0;
#undef M
  glMultMatrixf( m );

  /* Translate Eye to Origin */
  glTranslatef( -cam[0], -cam[1], -cam[2]);
}

void drawText(FontTex* ftx, int x, int y, int size, const char *text) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_TEXTURE_2D);

  glPushMatrix();

  glTranslatef(x, y, 0);
  glScalef(size, size, size);
  ftxRenderString(ftx, text, strlen(text));
  
  glPopMatrix();
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
  polycount += 2 * strlen(text); /* quads are two triangles */
}

char* readShaderFile(const char* shaderPath) {
  file_handle shaderFile;
  int shaderLength;
  char* shader;
  
  shaderFile = file_open(shaderPath, "r");
  file_seek(shaderFile, 0, SEEK_END);
  shaderLength = file_tell(shaderFile);
  shader = malloc(shaderLength + 1);
  file_rewind(shaderFile);
  file_read(shaderFile, shader, shaderLength);
  file_close(shaderFile);
  shader[shaderLength] = '\0';
  
  return shader;
}

unsigned int loadShaders(const char* vertexPath, const char* fragmentPath) {
  const char* vertexShader;
  const char* fragmentShader;
  unsigned int vertexID, fragmentID;
  unsigned int programID = 0;
  int result = GL_FALSE, infoLogLength;
  
  vertexShader = readShaderFile(vertexPath);
  fragmentShader = readShaderFile(fragmentPath);
  
  vertexID = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexID, 1, &vertexShader, NULL);
  glCompileShader(vertexID);
  glGetShaderiv(vertexID, GL_COMPILE_STATUS, &result);
  glGetShaderiv(vertexID, GL_INFO_LOG_LENGTH, &infoLogLength);
  if (result != GL_TRUE && infoLogLength > 0) {
    char errorMsg[infoLogLength];
    glGetShaderInfoLog(vertexID, infoLogLength, NULL, errorMsg);
    fprintf(stderr, "could not compile vertex shader:\n%s\n", errorMsg);
    goto FAIL_VERTEX;
  }
  
  fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentID, 1, &fragmentShader, NULL);
  glCompileShader(fragmentID);
  glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &result);
  glGetShaderiv(fragmentID, GL_INFO_LOG_LENGTH, &infoLogLength);
  if (result != GL_TRUE && infoLogLength > 0) {
    char errorMsg[infoLogLength];
    glGetShaderInfoLog(fragmentID, infoLogLength, NULL, errorMsg);
    fprintf(stderr, "could not compile fragment shader:\n%s\n", errorMsg);
    goto FAIL_FRAGMENT;
  }
  
  programID = glCreateProgram();
  glAttachShader(programID, vertexID);
  glAttachShader(programID, fragmentID);
  glLinkProgram(programID);
  glGetProgramiv(programID, GL_LINK_STATUS, &result);
  glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
  if (result != GL_TRUE && infoLogLength > 0) {
    char errorMsg[infoLogLength];
    glGetProgramInfoLog(programID, infoLogLength, NULL, errorMsg);
    fprintf(stderr, "could not link shader program:\n%s\n", errorMsg);
    glDeleteProgram(programID);
    programID = 0;
  }
  
FAIL_FRAGMENT:
  glDeleteShader(fragmentID);
FAIL_VERTEX:
  glDeleteShader(vertexID);
  
  return programID;
}
