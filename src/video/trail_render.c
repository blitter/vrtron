#include "video/trail_geometry.h"
								 
int trailState = 0;

void trailStatesNormal(Player *pPlayer, int texture) {
	trailState = 1;

	// glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	// glDisable(GL_TEXTURE_2D);
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	// glDisable(GL_LIGHTING);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

	{ 
		float black[] = { 0, 0, 0, 1 };
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, black);
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
	}

	if(gSettingsCache.alpha_trails) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
}

void trailStatesShadowed(void) {
	trailState = 0;

	// glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_LIGHTING);
}

void trailStatesRestore(void) {
	glDisable(GL_COLOR_MATERIAL);
	glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void trailRender(TrailMesh *pMesh) {
	int i;
  
	if(pMesh->iUsed == 0)
		return;
	
	glBegin(GL_TRIANGLES);
	for (i = 0; i < pMesh->iUsed; i++)
	{
		if (trailState) {
			glNormal3fv(&pMesh->pNormals[pMesh->pIndices[i]]);
			glTexCoord2fv(&pMesh->pTexCoords[pMesh->pIndices[i]]);
			glColor4ubv(&pMesh->pColors[pMesh->pIndices[i] * 4]);
		}
		glVertex3fv(&pMesh->pVertices[pMesh->pIndices[i]]);
	}
	glEnd();
  
	trailState = 0;

	checkGLError("trail");
}
