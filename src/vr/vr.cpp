#ifdef VR

extern "C" {
#if defined(__APPLE__)
#include <GLEW/GLEW.h>
#else
#ifdef GLEW_STATIC
#include "GL/glew.h"
#else
#include <GL/glew.h>
#endif
#endif

#include "SDL_syswm.h"

#include "vr/vr.h"
#include "filesystem/path.h"
}

#include "OVR.h"
#include "OVR_CAPI_GL.h"
#include "Util/Util_Render_Stereo.h"

using namespace OVR;

int vrEnabled;
int vrEye;
float vrTextureWidth;
float vrTextureHeight;

ovrHmd vrHMD;
ovrPosef vrHeadPose[2];
ovrGLConfig vrConfig;
ovrEyeRenderDesc vrEyeRenderDesc[2];
ovrGLTexture vrEyeTextures[2];

unsigned int vrFBO;
unsigned int vrTexture;
unsigned int vrDepthBuf;
float vrScaleFactorX;
float vrScaleFactorY;

void vrOrtho(float l, float r, float b, float t, float n, float f) {
  /* the SDK provides an ortho matrix in NDC space */
  /* convert NDC space to screen space, as glTron uses */
  /*Matrix4f orthoMat = Matrix4f(2.0f / (r - l), 0.0f, 0.0f, 0.0f,
                               0.0f, -2.0f / (t - b), 0.0f, 0.0f,
                               0.0f, 0.0f, -2.0f / (f - n), 0.0f,
                               -(r + l) / (r - l), (t + b) / (t - b), -(f + n) / (f - n), 1.0f);*/
  Matrix4f perspectiveMat = ovrMatrix4f_Projection(vrEyeRenderDesc[vrEye].Fov, 0.01f, 10000.f, true);
  Vector2f orthoScale = Vector2f(0.4f) / Vector2f(vrEyeRenderDesc[vrEye].PixelsPerTanAngleAtCenter);
  float width = r - l;
  float height = t - b;
  if (width > height) {
    orthoScale.x *= (4.0f * height / 3.0f) / width;
  } else if (height > width) {
    orthoScale.y *= (3.0f * width / 4.0f) / height;
  }
  orthoScale.y *= -1.0f;
  Matrix4f orthoMat = ovrMatrix4f_OrthoSubProjection(perspectiveMat, orthoScale, 1.5f, vrEyeRenderDesc[vrEye].ViewAdjust.x);
  orthoMat *= Matrix4f::Translation(-width / 2.0f + 150.0f, 0, 0); // it's already centered, but it doesn't *look* centered, so add an x offset
  orthoMat.Transpose();
  /*orthoMat *= vrStereoConfig.GetEyeRenderParams(vrEye ? StereoEye_Right : StereoEye_Left).OrthoProjection.Transposed();*/
  glMultMatrixf((GLfloat*)&orthoMat);
}

void vrViewport(float x, float y, float width, float height) {
  glViewport(x * vrScaleFactorX + (vrTextureWidth / 2.0f * vrEye), y * vrScaleFactorY, width / 2.0f * vrScaleFactorX, height * vrScaleFactorY);
}

void vrPerspective(float fov, float ratio, float znear, float zfar) {
  Matrix4f perspectiveMat = ovrMatrix4f_Projection(vrEyeRenderDesc[vrEye].Fov, znear, zfar, true);
  perspectiveMat.Transpose();
//  perspectiveMat.M[2][2] = -(zfar + znear) / (zfar - znear);
//  perspectiveMat.M[3][2] = -(2.0f * zfar * znear) / (zfar - znear);
  glMultMatrixf((GLfloat*)&perspectiveMat);
}

void vrLookAt(float *cam, float *target, float *up) {

  Matrix4f shiftMat = Matrix4f::Translation(vrEyeRenderDesc[vrEye].ViewAdjust).Transposed();
  Matrix4f transMat = Matrix4f::Translation(-vrHeadPose[vrEye].Position.x * 2.5f, -vrHeadPose[vrEye].Position.y * 2.5f, -(vrHeadPose[vrEye].Position.z * 2.5f + 10.0f)).Transposed();
  Matrix4f orientMat = Matrix4f(Quatf(vrHeadPose[vrEye].Orientation));
  //orientMat *= Matrix4f::RotationX(-M_PI / 6.0f).Transposed();
  glMultMatrixf((GLfloat*)&orientMat);
  glMultMatrixf((GLfloat*)&transMat);
  glMultMatrixf((GLfloat*)&shiftMat);
  doLookAt(cam, target, up);
}

void initVR(void) {
  GLenum err;
  
  GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0_EXT };
  
  vrEnabled = 0;
  
  /* enumerate VR devices */
  ovr_Initialize();
  
  vrHMD = ovrHmd_Create(0);
  /*if (!vrHMD) {
    vrHMD = ovrHmd_CreateDebug(ovrHmd_DK2);
  }*/
  if (!vrHMD) {
    // values for the DK1
    /*	HResolution: 1280, VResolution: 800
				HScreenSize: 0.149760, VScreenSize: 0.093600
        VScreenCenter: 0.046800
        EyeToScreenDistance: 0.041000
        LensSeparationDistance: 0.063500
        InterpupillaryDistance: 0.064000
        DistortionK[4]: 1.000000, 0.220000, 0.240000, 0.000000
        ChromaAbCorrection[4]: 0.996000, -0.004000, 1.014000, 0.000000
     */
    fprintf(stderr, "could not get vr device\n");
    ovr_Shutdown();
    return;
  }
  
  if (!ovrHmd_ConfigureTracking(vrHMD, ovrTrackingCap_Orientation |
                     ovrTrackingCap_MagYawCorrection |
									   ovrTrackingCap_Position, 0)) {
    fprintf(stderr, "could not configure tracking sensors\n");
    ovrHmd_Destroy(vrHMD);
    ovr_Shutdown();
    return;
  }
  
  /* setup VR visuals */
  Sizei recommendedTex0Size = ovrHmd_GetFovTextureSize(vrHMD, ovrEye_Left,
                                                       vrHMD->DefaultEyeFov[0], 1.0f);
  Sizei recommendedTex1Size = ovrHmd_GetFovTextureSize(vrHMD, ovrEye_Right,
                                                       vrHMD->DefaultEyeFov[1], 1.0f);
  vrTextureWidth = recommendedTex0Size.w + recommendedTex1Size.w;
  vrTextureHeight = recommendedTex0Size.h + recommendedTex1Size.h;
  vrScaleFactorX = vrTextureWidth / gScreen->vp_w;//vrHMD->Resolution.w;
  vrScaleFactorY = vrScaleFactorX * ((float)gScreen->vp_w / (float)gScreen->vp_h);//vrTextureHeight / gScreen->vp_h;//vrHMD->Resolution.h;

  SDL_SysWMinfo info;
  SDL_VERSION(&info.version);
  if (!SDL_GetWMInfo(&info)) {
    fprintf(stderr, "could not get hwnd from sdl\n");
    glDeleteFramebuffersEXT(1, (GLuint*) &vrFBO);
    glDeleteTexturesEXT(1, (GLuint*) &vrTexture);
    glDeleteRenderbuffersEXT(1, (GLuint*) &vrDepthBuf);
    ovrHmd_Destroy(vrHMD);
    ovr_Shutdown();
    return;
  }
  
  vrConfig.OGL.Header.API = ovrRenderAPI_OpenGL;
  vrConfig.OGL.Header.RTSize = Sizei(gScreen->vp_w, gScreen->vp_h);//vrHMD->Resolution;
// multisampling currently isn't used by the Oculus SDK as of 9/12/14
  vrConfig.OGL.Header.Multisample = 1;
  vrConfig.OGL.Window = info.window;
  vrConfig.OGL.DC = GetDC(vrConfig.OGL.Window);
  
  ovrHmd_ConfigureRendering(vrHMD, &vrConfig.Config,  ovrDistortionCap_Chromatic | ovrDistortionCap_TimeWarp | ovrDistortionCap_Overdrive, vrHMD->DefaultEyeFov, vrEyeRenderDesc);
  if (!(vrHMD->HmdCaps & ovrHmdCap_ExtendDesktop)) {
    ovrHmd_AttachToWindow(vrHMD, info.window, 0, 0);
  }
  
  /* setup GLEW */
  err = glewInit();
  if (err != GLEW_OK) {
    fprintf(stderr, "could not initialize glew\n");
    ovrHmd_Destroy(vrHMD);
    ovr_Shutdown();
    return;
  }

  if (!GLEW_VERSION_2_0) {
    fprintf(stderr, "could not find gl version 2 or later\n");
    ovrHmd_Destroy(vrHMD);
    ovr_Shutdown();
    return;
  }
    
  glGenFramebuffersEXT(1, (GLuint*) &vrFBO);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, vrFBO);
  
  glGenTexturesEXT(1, (GLuint*) &vrTexture);
  glBindTexture(GL_TEXTURE_2D, vrTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, vrTextureWidth, vrTextureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  
  glGenRenderbuffersEXT(1, (GLuint*) &vrDepthBuf);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, vrDepthBuf);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, vrTextureWidth, vrTextureHeight);
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, vrDepthBuf);
  
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, vrTexture, 0);
  glDrawBuffers(1, drawBuffers);
  
  if (glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT) {
    fprintf(stderr, "could not set up vr framebuffer\n");
    glDeleteFramebuffersEXT(1, (GLuint*) &vrFBO);
    glDeleteTexturesEXT(1, (GLuint*) &vrTexture);
    glDeleteRenderbuffersEXT(1, (GLuint*) &vrDepthBuf);
    ovrHmd_Destroy(vrHMD);
    ovr_Shutdown();
    return;
  }
  
  vrEyeTextures[0].OGL.Header.API = ovrRenderAPI_OpenGL;
  vrEyeTextures[0].OGL.Header.TextureSize = Sizei(vrTextureWidth, vrTextureHeight);
  vrEyeTextures[0].OGL.Header.RenderViewport = Recti(0, 0, vrEyeTextures[0].OGL.Header.TextureSize.w / 2, vrEyeTextures[0].OGL.Header.TextureSize.h);
  vrEyeTextures[0].OGL.TexId = vrTexture;
  vrEyeTextures[1] = vrEyeTextures[0];
  vrEyeTextures[1].OGL.Header.RenderViewport.Pos.x = vrEyeTextures[0].OGL.Header.TextureSize.w / 2;
  
  orthoFunc = vrOrtho;
  viewportFunc = vrViewport;
  perspectiveFunc = vrPerspective;
  lookAtFunc = vrLookAt;
  
  vrEnabled = 1;
}

void shutdownVR(void) {
  glDeleteFramebuffersEXT(1, (GLuint*) &vrFBO);
  glDeleteTexturesEXT(1, (GLuint*) &vrTexture);
  glDeleteRenderbuffersEXT(1, (GLuint*) &vrDepthBuf);
  
  ovrHmd_Destroy(vrHMD);
  ovr_Shutdown();
  
  orthoFunc = doOrtho;
  viewportFunc = doViewport;
  perspectiveFunc = doPerspective;
  lookAtFunc = doLookAt;
  
  vrEnabled = 0;
}

void setupVRGL(Visual* d) {
  ovrHSWDisplayState hswDisplayState;
  ovrHmd_GetHSWDisplayState(vrHMD, &hswDisplayState);
  if (hswDisplayState.Displayed) {
    ovrHmd_DismissHSWDisplay(vrHMD);
  }
  
  ovrHmd_BeginFrame(vrHMD, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, vrFBO);
  
  if (vrEye == 0) {
    for (int eyeIndex = 0; eyeIndex < ovrEye_Count; ++eyeIndex) {
      ovrEyeType eye = vrHMD->EyeRenderOrder[eyeIndex];
      vrHeadPose[eye] = ovrHmd_GetEyePose(vrHMD, eye);
    }
  }
}

void finishVRGL(Visual* d) {
  if (vrEye != 0) {  
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    ovrHmd_EndFrame(vrHMD, vrHeadPose, (const ovrTexture*)vrEyeTextures);
  }
  vrEye ^= 1;
}

#endif
